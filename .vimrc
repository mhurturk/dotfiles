" Make backspace behave like every other editor.
set backspace=indent,eol,start

"========THEME
" Enable syntax coloring
syntax enable
" Set color scheme
colorscheme elflord
" Activate line numbers
set number

"========SEARCHING
" Enable search highlighting
set hlsearch
" Enable incremental interactive searching
set incsearch

"========MAPPINGS
" Make editing .vimrc file easier from anywhere
nmap <Leader>ev :tabedit $MYVIMRC<cr>
" Add simple highlight removal for search
nmap <Leader><space> :nohlsearch<cr>
" Close current tab easier
nmap <Leader>tc :tabc<cr>

"========AUTO-COMMANDS
" Automatically source the .vimrc file on save
augroup autosourcing
	autocmd!
	autocmd BufWritePost .vimrc source %
augroup END
